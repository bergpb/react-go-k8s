## Go API and React UI on Kubernetes (kind)

Simple applications to study more about service communication inside a Kubernetes cluster

#### Requirements:
`kind` cluster running with nginx ingress: [kind-ingress-nginx](https://kind.sigs.k8s.io/docs/user/ingress/#ingress-nginx)

#### How to run:
  1. Build docker images:
        ```
        docker build -t go-api:latest api/.
        docker build -t react-ui:latest ui/.
        ```
  2. Load images to kind:
        ```
        kind load docker-image go-api:latest
        kind load docker-image react-ui:latest
        ```
  3. Deploy manifests using kubectl:
        ```
        kubectl apply -f deployment.yaml
        kubectl apply -f services.yaml
        kubectl apply -f ingress.yaml
        ```
  4. The applications will be available in [http://localhost](http://localhost)
    - UI  - [http://localhost]
    - API - [http://localhost/api/ping]